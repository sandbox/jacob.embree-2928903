<?php

/**
 * @file
 * Functions for integrating with the Devel module.
 */

/**
 * Provides invite menu items.
 *
 * @see devel_menu()
 */
function invite_devel_menu() {
  $items['invite/%invite/devel'] = array(
    'title' => 'Devel',
    'page callback' => 'devel_load_object',
    'page arguments' => array('invite', 1),
    'access arguments' => array('access devel information'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'devel.pages.inc',
    'file path' => drupal_get_path('module', 'devel'),
    'weight' => 100,
  );
  $items['invite/%invite/devel/load'] = array(
    'title' => 'Load',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['invite/%invite/devel/render'] = array(
    'title' => 'Render',
    'page callback' => 'invite_devel_render_object',
    'page arguments' => array('invite', 1),
    'access arguments' => array('access devel information'),
    'file' => 'invite.devel.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 100,
  );
  return $items;
}

/**
 * Provides invite admin paths pertaining to Devel.
 *
 * @see devel_admin_paths()
 */
function invite_devel_admin_paths() {
  $paths = array(
    'invite/*/devel' => TRUE,
    'invite/*/devel/*' => TRUE,
  );
  return $paths;
}

/**
 * Page callback: Wraps devel_render_object().
 */
function invite_devel_render_object($type, $object, $name = NULL) {
  module_load_include('inc', 'invite', 'includes/invite.pages');
  module_load_include('inc', 'devel', 'devel.pages');
  return devel_render_object($type, $object, $name);
}
