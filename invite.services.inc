<?php

/**
 * @file
 * Services integration for Invite.
 */

/**
 * Implements hook_services_resources().
 */
function invite_services_resources() {
  $invite_resource = array(
    'invite' => array(
      'operations' => array(
        'retrieve' => array(
          'help' => 'Retrieve an invite',
          'file' => array(
            'type' => 'inc',
            'module' => 'invite',
            'name' => 'invite.services',
          ),
          'callback' => 'invite_resource_retrieve',
          'args' => array(
            array(
              'name' => 'iid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The iid of the invite to retrieve',
            ),
          ),
          'access callback' => 'invite_resource_access',
          'access arguments' => array('view'),
          'access arguments append' => TRUE,
        ),
        'create' => array(
          'help' => 'Create an invite',
          'file' => array(
            'type' => 'inc',
            'module' => 'invite',
            'name' => 'invite.services',
          ),
          'callback' => 'invite_resource_create',
          'args' => array(
            array(
              'name' => 'invite',
              'optional' => FALSE,
              'source' => 'data',
              'description' => 'The invite data to create',
              'type' => 'array',
            ),
          ),
          'access callback' => 'invite_resource_access',
          'access arguments' => array('create'),
          'access arguments append' => TRUE,
        ),
        'update' => array(
          'help' => 'Update an invite',
          'file' => array(
            'type' => 'inc',
            'module' => 'invite',
            'name' => 'invite.services',
          ),
          'callback' => 'invite_resource_update',
          'args' => array(
            array(
              'name' => 'iid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The iid of the invite to update',
            ),
            array(
              'name' => 'invite',
              'optional' => FALSE,
              'source' => 'data',
              'description' => 'The invite data to update',
              'type' => 'array',
            ),
          ),
          'access callback' => 'invite_resource_access',
          'access arguments' => array('update'),
          'access arguments append' => TRUE,
        ),
        'delete' => array(
          'help' => t('Delete an invite'),
          'file' => array(
            'type' => 'inc',
            'module' => 'invite',
            'name' => 'invite.services',
          ),
          'callback' => 'invite_resource_delete',
          'args' => array(
            array(
              'name' => 'iid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The iid of the invite to delete',
            ),
          ),
          'access callback' => 'invite_resource_access',
          'access arguments' => array('delete'),
          'access arguments append' => TRUE,
        ),
        'index' => array(
          'help' => 'List all invites',
          'file' => array(
            'type' => 'inc',
            'module' => 'invite',
            'name' => 'invite.services',
          ),
          'callback' => 'invite_resource_index',
          'args' => array(
            array(
              'name' => 'page',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'The zero-based index of the page to get, defaults to 0.',
              'default value' => 0,
              'source' => array('param' => 'page'),
            ),
            array(
              'name' => 'fields',
              'optional' => TRUE,
              'type' => 'string',
              'description' => 'The fields to get.',
              'default value' => '*',
              'source' => array('param' => 'fields'),
            ),
            array(
              'name' => 'parameters',
              'optional' => TRUE,
              'type' => 'array',
              'description' => 'Parameters array',
              'default value' => array(),
              'source' => array('param' => 'parameters'),
            ),
            array(
              'name' => 'pagesize',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'Number of records to get per page.',
              'default value' => variable_get('services_invite_index_page_size', 20),
              'source' => array('param' => 'pagesize'),
            ),
            array(
              'name' => 'options',
              'optional' => TRUE,
              'type' => 'array',
              'description' => 'Additional query options.',
              'default value' => array(
                'orderby' => array(
                  'created' => 'DESC'
                )
              ),
              'source' => array('param' => 'options'),
            ),
          ),
          'access arguments' => array('access content'),
        ),
      ),
    ),
  );
  return $invite_resource;
}

/**
 * Returns the results of an invite_load() for the specified invite.
 *
 * This returned invite may optionally take content_permissions settings into
 * account, based on a configuration setting.
 *
 * @param $iid
 *   IID of the invite we want to return.
 *
 * @return
 *   Invite object or FALSE if not found.
 *
 * @see invite_load()
 */
function invite_resource_retrieve($iid) {
  $invite = invite_load($iid);

  if ($invite) {
    $uri = entity_uri('invite', $invite);
    $invite->path = url($uri['path'], array('absolute' => TRUE));
    // Unset uri as it has complete entity and this
    // cause never ending recursion in rendering.
    unset($invite->uri);
  }
  // Let's check field_permissions
  $invite = services_field_permissions_clean('view', 'invite', $invite);
  return $invite;
}

/**
 * Creates a new invite based on submitted values.
 *
 * @param $invite
 *   Array representing the attributes an invite edit form would submit.
 *
 * @return
 *   An associative array contained the new invite's iid and, if applicable,
 *   the fully qualified URI to this resource.
 *
 * @see drupal_form_submit()
 */
function invite_resource_create($invite) {
  global $user;
  // Adds backwards compatability with regression fixed in #1083242
  $invite = _services_arg_value($invite, 'invite');
  if (!isset($invite['uid'])) {
    $invite['uid'] = $user->uid;
  }
  // Validate the invite. If there is validation error Exception will be thrown
  // so code below won't be executed.
  invite_resource_validate_type($invite);

  // Load the required includes for drupal_form_submit
  module_load_include('inc', 'invite', 'includes/invite.admin');

  $invite_type = $invite['type'];

  // Setup form_state
  $form_state = array();
  $form_state['values'] = $invite;
  $form_state['values']['op'] = variable_get('services_invite_save_button_' . $invite_type . '_resource_create', t('Save'));
  $form_state['programmed_bypass_access_check'] = FALSE;
  $form_state['no_cache'] = TRUE;

  // Build a stub invite object for the form in a similar way as invite_add()
  // does.
  $stub_invite = entity_create('invite', array('type' => $invite_type));

  // Contributed modules may check the triggering element in the form state.
  // The triggering element is usually set from the browser's POST request, so
  // we'll automatically set it as the submit action from here.
  $stub_form_state = array(
    'no_cache' => TRUE,
    'build_info' => array(
      'args' => array((object) $stub_invite),
    ),
  );
  $stub_form = drupal_build_form('invite_form', $stub_form_state);
  $form_state['triggering_element'] = $stub_form['actions']['submit'];

  drupal_form_submit('invite_form', $form_state, (object)$stub_invite);

  if ($errors = form_get_errors()) {
    return services_error(implode(" ", $errors), 406, array('form_errors' => $errors));
  }

  $invite = array('iid' => $form_state['invite']->iid);
  if ($uri = services_resource_uri(array('invite', $invite['iid']))) {
    $invite['uri'] = $uri;
  }
  return $invite;
}

/*
 * Helper function to validate invite type information.
 *
 * @param $invite
 *   Array representing the attributes an invite edit form would submit.
 */
function invite_resource_validate_type($invite) {
  if (!isset($invite['type'])) {
    return services_error(t('Missing invite type'), 406);
  }
  // Wanted to return a graceful error instead of a blank iid, this should
  // allow for that.
  $types = invite_get_types();
  $invite_type = $invite['type'];
  if (!isset($types[$invite_type])) {
    return services_error(t('Invite type @type does not exist.', array('@type' => $invite_type)), 406);
  }
  $allowed_invite_types = variable_get('services_allowed_create_content_types', $types);
  if (!isset($allowed_invite_types[$invite_type])) {
    return services_error(t("This invite type @type can't be processed via services", array('@type' => $invite_type)), 406);
  }
}

/**
 * Updates a new invite based on submitted values.
 *
 * @param $iid
 *   Invite ID of the invite we're editing.
 * @param $invite
 *   Array representing the attributes an invite edit form would submit.
 *
 * @return
 *   The invite's iid.
 *
 * @see drupal_form_submit()
 */
function invite_resource_update($iid, $invite) {
  // Adds backwards compatability with regression fixed in #1083242
  $invite = _services_arg_value($invite, 'invite');

  $invite['iid'] = $iid;

  $old_invite = invite_load($iid);
  if (empty($old_invite->iid)) {
    return services_error(t('Invite @iid not found', array('@iid' => $old_invite->iid)), 404);
  }

  // If no type is provided use the existing invite type.
  if (empty($invite['type'])) {
    $invite['type'] = $old_invite->type;
  }
  elseif ($invite['type'] != $old_invite->type) {
    // Invite types cannot be changed once they are created.
    return services_error(t('Invite type cannot be changed'), 406);
  }

  // Validate the invite. If there is validation error Exception will be thrown
  // so code below won't be executed.
  invite_resource_validate_type($invite);

  // Load the required includes for drupal_form_submit
  module_load_include('inc', 'invite', 'includes/invite.admin');

  $invite_type = $invite['type'];

  // Setup form_state.
  $form_state = array();
  $form_state['values'] = $invite;
  $form_state['values']['op'] = variable_get('services_invite_save_button_' . $invite_type . '_resource_update', t('Save'));
  $form_state['invite'] = $old_invite;
  $form_state['programmed_bypass_access_check'] = FALSE;

  // Contributed modules may check the triggering element in the form state.
  // The triggering element is usually set from the browser's POST request, so
  // we'll automatically set it as the submit action from here.
  $stub_form = drupal_get_form('invite_form', (object) $old_invite);
  $form_state['triggering_element'] = $stub_form['actions']['submit'];

  drupal_form_submit('invite_form', $form_state, $old_invite);

  if ($errors = form_get_errors()) {
    return services_error(implode(" ", $errors), 406, array('form_errors' => $errors));
  }

  $invite = array('iid' => $iid);
  if ($uri = services_resource_uri(array('invite', $iid))) {
    $invite['uri'] = $uri;
  }
  return $invite;
}

/**
 * Delete an invite given its iid.
 *
 * @param int $iid
 *   Invite ID of the invite we're deleting.
 *
 * @return bool
 *   Always returns true.
 */
function invite_resource_delete($iid) {
  invite_delete(invite_load($iid));
  return TRUE;
}

/**
 * Return an array of optionally paged iid's based on a set of criteria.
 *
 * An example request might look like
 *
 * http://domain/endpoint/invite?fields=iid,vid&parameters[iid]=7&parameters[uid]=1
 *
 * This would return an array of objects with only iid and vid defined, where
 * iid = 7 and uid = 1.
 *
 * @param $page
 *   Page number of results to return (in pages of 20).
 * @param $fields
 *   The fields you want returned.
 * @param $parameters
 *   An array containing fields and values used to build a sql WHERE clause
 *   indicating items to retrieve.
 * @param $page_size
 *   Integer number of items to be returned.
 * @param $options
 *   Additional query options.
 *
 * @return
 *   An array of invite objects.
 *
 * @todo
 *   Evaluate the functionality here in general. Particularly around
 *     - Do we need fields at all? Should this just return full invites?
 *     - Is there an easier syntax we can define which can make the urls
 *       for index requests more straightforward?
 */
function invite_resource_index($page, $fields, $parameters, $page_size, $options = array()) {
  $invite_select = db_select('invite', 't')
    ->addTag('invite_access');

  services_resource_build_index_query($invite_select, $page, $fields, $parameters, $page_size, 'invite', $options);

  if (!user_access('administer invitations')) {
    $invite_select->condition('status', 1);
  }

  $results = services_resource_execute_index_query($invite_select);

  return services_resource_build_index_list($results, 'invite', 'iid');
}

/**
 * Determine whether the current user can access an invite resource.
 *
 * @param $op
 *   One of view, update, create, delete per invite_access().
 * @param $args
 *   Resource arguments passed through from the original request.
 *
 * @return bool
 *
 * @see invite_access()
 */
function invite_resource_access($op = 'view', $args = array()) {
  // Adds backwards compatability with regression fixed in #1083242
  if (isset($args[0])) {
    $args[0] = _services_access_value($args[0], 'invite');
  }

  // Make sure we have an object or this all fails, some servers can
  // mess up the types.
  if (is_array($args[0])) {
    $args[0] = (object) $args[0];
  }
  elseif (!is_array($args[0]) && !is_object($args[0])) {  //This is to determine if it is just a string happens on invite/%IID
    $args[0] = (object)array('iid' => $args[0]);
  }

 if ($op != 'create' && !empty($args)) {
    $invite = invite_load($args[0]->iid);
  }
  elseif ($op == 'create') {
    if (isset($args[0]->type)) {
      $invite = $args[0]->type;
      return invite_access($op, $invite);
    }
    else {
      return services_error(t('Invite type is required'), 406);
    }
  }
  if (isset($invite->iid) && $invite->iid) {
    return invite_access($op, $invite);
  }
  else {
    return services_error(t('Invite @iid could not be found', array('@iid' => $args[0]->iid)), 404);
  }
}

/**
 * Helper function to validate data.
 *
 * @param $op
 *   Array representing the attributes an invite edit form would submit.
 * @param $args
 *   Resource arguments passed through from the original request (invite_type,
 *   field_name).
 *
 * @return bool
 *   TRUE/FALSE based on access.
 */
function invite_resource_validate_invite_type_field_name($op = 'create', $args = array()) {
  $invite_type = $args[0];
  $field_name = $args[1];

  $temp_invite = array('type' => $invite_type);

  // An invalid invite type throws an exception, and stops before the return below.
  invite_resource_validate_type($temp_invite);

  if (!field_info_instance('invite', $field_name, $invite_type)) {
    return services_error(t('Field name \'@field_name\' not found on invite type \'@invite_type\'', array('@field_name' => $field_name, '@invite_type' => $invite_type)), 406);
  }

  return TRUE;
}
